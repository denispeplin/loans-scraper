require 'selenium-webdriver'
require 'nokogiri'

def nokogiri_doc(url)
  browser = Selenium::WebDriver.for :firefox
  browser.navigate.to url

  html = browser.page_source
  browser.quit

  doc = Nokogiri::HTML html
end

def minmax(values)
  result = values.flatten.map{ |value| Float value }.sort

  [result.first, result.last]
end

def scrape_page1
  doc = nokogiri_doc 'https://www.meetearnest.com/student/#/check-rate'

  minmax doc.css('#profile-widget .rate').map { |row| row.text.scan(/\d\.[\d]{2}/) }
end

def scrape_page2
  doc = nokogiri_doc 'https://studentrefi.lendkey.com/home/rates_and_fees'

  minmax doc.css('#body_content table tr')[1..-1].map { |row| row.css('td').last.text.scan(/\d\.[\d]{2}/)[0..1] }
end

results = [scrape_page1, scrape_page2] # .sort_by(&:first)

results.each do |result|
  puts '%.2f%% - %.2f%%' % result
end

puts '>>> %.2f%% - %.2f%%' % results.sort_by(&:first).first
